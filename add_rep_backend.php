<?php
require_once 'admin_check.php';
?>
<?php
require_once 'include/database.php';

if(isset($_POST))
{
	$username=$_POST['student_name'];
	$collegeid=$_POST['college_name'];
	
	$stmt=$dbh->prepare("insert into college_users_assoc(user_id,college_id) values(:user_id,:college_id);");
	$stmt->bindParam(':user_id',$username);
	$stmt->bindParam(':college_id',$collegeid);
	
	if($stmt->execute())
	{

		echo "User Associated With College";
	}
	else
	{

		echo "Failed";

	}
}

?>