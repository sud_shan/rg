<?php
session_start();
?>
<html>
<head>
	<style>
		.rotate {
			filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
			-ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
			-moz-transform: rotate(-90.0deg);  /* FF3.5+ */
			-ms-transform: rotate(-90.0deg);  /* IE9+ */
			-o-transform: rotate(-90.0deg);  /* Opera 10.5 */
			-webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
			transform: rotate(-90.0deg);  /* Standard */
		}
		@font-face
		{font-family:"Cambria Math";
		panose-1:2 4 5 3 5 4 6 3 2 4;}
		@font-face
		{font-family:"Segoe UI";
		panose-1:2 11 5 2 4 2 4 2 2 3;}
		/* Style Definitions */
		p.MsoNormal, li.MsoNormal, div.MsoNormal
		{margin-top:10.0pt;
			margin-right:0cm;
			margin-bottom:0cm;
			margin-left:0cm;
			margin-bottom:.0001pt;
			line-height:115%;
			font-size:10.0pt;
			font-family:"Times New Roman","serif";
			color:black;}
			h1
			{mso-style-link:"Heading 1 Char";
			margin-top:18.0pt;
			margin-right:0cm;
			margin-bottom:0cm;
			margin-left:0cm;
			margin-bottom:.0001pt;
			page-break-after:avoid;
			background:#B53D68;
			border:none;
			padding:0cm;
			font-size:11.0pt;
			font-family:"Arial","sans-serif";
			color:white;
			text-transform:uppercase;}
			h2
			{mso-style-link:"Heading 2 Char";
			margin-top:12.0pt;
			margin-right:0cm;
			margin-bottom:12.0pt;
			margin-left:0cm;
			page-break-after:avoid;
			background:#F1D7E0;
			border:none;
			padding:0cm;
			font-size:11.0pt;
			font-family:"Times New Roman","serif";
			color:black;
			text-transform:uppercase;
			font-weight:normal;}
			h3
			{mso-style-link:"Heading 3 Char";
			margin-top:12.0pt;
			margin-right:0cm;
			margin-bottom:3.0pt;
			margin-left:0cm;
			page-break-after:avoid;
			border:none;
			padding:0cm;
			font-size:11.0pt;
			font-family:"Times New Roman","serif";
			color:#5A1E34;
			text-transform:uppercase;
			font-weight:normal;}
			p.MsoHeader, li.MsoHeader, div.MsoHeader
			{mso-style-link:"Header Char";
			margin-top:10.0pt;
			margin-right:0cm;
			margin-bottom:0cm;
			margin-left:0cm;
			margin-bottom:.0001pt;
			font-size:10.0pt;
			font-family:"Times New Roman","serif";
			color:black;}
			p.MsoFooter, li.MsoFooter, div.MsoFooter
			{mso-style-link:"Footer Char";
			margin-top:0cm;
			margin-right:0cm;
			margin-bottom:0cm;
			margin-left:-36.0pt;
			margin-bottom:.0001pt;
			font-size:10.0pt;
			font-family:"Times New Roman","serif";
			color:black;}
			span.MsoPageNumber
			{font-weight:bold;}
			p.MsoListBullet, li.MsoListBullet, div.MsoListBullet
			{margin-top:6.0pt;
				margin-right:0cm;
				margin-bottom:0cm;
				margin-left:10.8pt;
				margin-bottom:.0001pt;
				text-indent:-10.8pt;
				line-height:110%;
				font-size:10.0pt;
				font-family:"Times New Roman","serif";
				color:#505050;}
				p.MsoTitle, li.MsoTitle, div.MsoTitle
				{mso-style-link:"Title Char";
				margin:0cm;
				margin-bottom:.0001pt;
				font-size:36.0pt;
				font-family:"Arial","sans-serif";
				color:black;}
				p.MsoTitleCxSpFirst, li.MsoTitleCxSpFirst, div.MsoTitleCxSpFirst
				{mso-style-link:"Title Char";
				margin:0cm;
				margin-bottom:.0001pt;
				font-size:36.0pt;
				font-family:"Arial","sans-serif";
				color:black;}
				p.MsoTitleCxSpMiddle, li.MsoTitleCxSpMiddle, div.MsoTitleCxSpMiddle
				{mso-style-link:"Title Char";
				margin:0cm;
				margin-bottom:.0001pt;
				font-size:36.0pt;
				font-family:"Arial","sans-serif";
				color:black;}
				p.MsoTitleCxSpLast, li.MsoTitleCxSpLast, div.MsoTitleCxSpLast
				{mso-style-link:"Title Char";
				margin:0cm;
				margin-bottom:.0001pt;
				font-size:36.0pt;
				font-family:"Arial","sans-serif";
				color:black;}
				p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
				{mso-style-link:"Body Text Char";
				margin:0cm;
				margin-bottom:.0001pt;
				text-align:center;
				font-size:10.0pt;
				font-family:"Times New Roman","serif";
				color:white;}
				p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
				{mso-style-link:"Balloon Text Char";
				margin-top:10.0pt;
				margin-right:0cm;
				margin-bottom:0cm;
				margin-left:0cm;
				margin-bottom:.0001pt;
				font-size:9.0pt;
				font-family:"Segoe UI","sans-serif";
				color:black;}
				span.MsoPlaceholderText
				{color:gray;}
				p.MsoQuote, li.MsoQuote, div.MsoQuote
				{mso-style-link:"Quote Char";
				margin-top:10.0pt;
				margin-right:72.0pt;
				margin-bottom:4.0pt;
				margin-left:72.0pt;
				line-height:115%;
				border:none;
				padding:0cm;
				font-size:10.0pt;
				font-family:"Times New Roman","serif";
				color:#A93B61;
				font-style:italic;}
				p.MsoTocHeading, li.MsoTocHeading, div.MsoTocHeading
				{margin-top:12.0pt;
					margin-right:0cm;
					margin-bottom:0cm;
					margin-left:0cm;
					margin-bottom:.0001pt;
					page-break-after:avoid;
					background:#B53D68;
					border:none;
					padding:0cm;
					font-size:11.0pt;
					font-family:"Arial","sans-serif";
					color:white;
					text-transform:uppercase;
					font-weight:bold;}
					span.BalloonTextChar
					{mso-style-name:"Balloon Text Char";
					mso-style-link:"Balloon Text";
					font-family:"Segoe UI","sans-serif";}
					span.TitleChar
					{mso-style-name:"Title Char";
					mso-style-link:Title;
					font-family:"Arial","sans-serif";}
					p.ContactInfo, li.ContactInfo, div.ContactInfo
					{mso-style-name:"Contact Info";
					margin:0cm;
					margin-bottom:.0001pt;
					line-height:125%;
					font-size:14.0pt;
					font-family:"Times New Roman","serif";
					color:black;}
					span.BodyTextChar
					{mso-style-name:"Body Text Char";
					mso-style-link:"Body Text";
					color:white;}
					span.HeaderChar
					{mso-style-name:"Header Char";
					mso-style-link:Header;}
					span.FooterChar
					{mso-style-name:"Footer Char";
					mso-style-link:Footer;}
					span.Heading1Char
					{mso-style-name:"Heading 1 Char";
					mso-style-link:"Heading 1";
					font-family:"Arial","sans-serif";
					color:white;
					text-transform:uppercase;
					background:#B53D68;
					font-weight:bold;}
					span.Heading2Char
					{mso-style-name:"Heading 2 Char";
					mso-style-link:"Heading 2";
					text-transform:uppercase;
					background:#F1D7E0;}
					span.Heading3Char
					{mso-style-name:"Heading 3 Char";
					mso-style-link:"Heading 3";
					color:#5A1E34;
					text-transform:uppercase;}
					span.QuoteChar
					{mso-style-name:"Quote Char";
					mso-style-link:Quote;
					color:#A93B61;
					font-style:italic;}
					.MsoChpDefault
					{font-size:10.0pt;
						color:black;}
						.MsoPapDefault
						{margin-top:10.0pt;
							line-height:115%;}
							/* Page Definitions */
							@page Section1
							{size:612.0pt 792.0pt;
								margin:42.55pt 180.0pt 42.55pt 72.0pt;
								border:solid windowtext 1.0pt;
								padding:28.0pt 28.0pt 28.0pt 28.0pt;}
								div.Section1
								{page:Section1;}
								/* List Definitions */
								ol
								{margin-bottom:0cm;}
								ul
								{margin-bottom:0cm;}
								.div1
								{
									border:2px solid #a1a1a1;
									padding:20px 50px; 

									width:1200px;
									border-radius:25px;
								}
							</style>
							<script type="text/javascript" src="https://www.google.com/jsapi"></script>

						</head><body>
						<?php 

						require_once 'include/database.php';


						$id=$_GET['id'];


						$name=$_GET['name'];

						?>

						<?php
						$sql="select distinct(name) from performance_".$id." ";
						$stmt=$dbh->prepare($sql);
						$stmt->execute();
//$r=$stmt->fetch();
						$i=1;
						$total_user=$stmt->rowCount();
						$sql1="select * from performance_".$id."  where name='$name' order by section_id asc";
						$stmt1=$dbh->prepare($sql1);
						$stmt1->execute();
						$sql5="select sum(section_score) from performance_".$id."  where name='$name'";
						$stmt5=$dbh->prepare($sql5);
						$stmt5->execute();
						$r5=$stmt5->fetch();
						$tot=$r5[0];

						$sql5="select sum(section_score) from performance_".$id."  group by name having sum(section_score)>$tot;";
						$stmt5=$dbh->prepare($sql5);
						$stmt5->execute();
//$r5=$stmt5->fetch();
						$num=$stmt5->rowCount();


//echo $sec_id;
						$x=array();
						$x1=array();
						?>
						
						<?php
						while($r1=$stmt1->fetch())
							{$sec_id=$r1['section_id'];
//	print_r($r1);
						$values=array();

						$values1=array();

						$sql2="select avg(section_score) from performance_".$id." where section_id='$sec_id'";
						$stmt2=$dbh->prepare($sql2);
						$stmt2->execute();
						$r2=$stmt2->fetch();
						$score=$r1['section_score'];
						$sql3="select * from performance_".$id."  where section_id='$sec_id' and section_score>'$score'";
						$stmt3=$dbh->prepare($sql3);
						$stmt3->execute();
						$r3=$stmt3->rowCount();
						$sql4="select * from performance_".$id."  where section_id='$sec_id' ";
						$stmt4=$dbh->prepare($sql4);
						$stmt4->execute();
						$r4=$stmt4->rowCount();
						if(!isset($per))
							$per=0;
						$per+=$r1['section_score'];

						?>


						<?php
						array_push($values1, $sec_id);
						array_push($values1, round(((($r4-$r3)/$r4)*100),2));
						array_push($values1, round(((($r4-$r3)/$r4)*100),2));
						array_push($x1, $values1);
						array_push($values, $sec_id);
						array_push($values, round(((int)$r1['section_score']/$r1['section_total'])*100,2));
						array_push($values, round(((int)$r1['section_score']/$r1['section_total'])*100,2));
						array_push($values, round(((float)$r2[0]/$r1['section_total'])*100,2));
						array_push($values, round(((float)$r2[0]/$r1['section_total'])*100,2));
						array_push($x, $values);
					}


					?>
					<?php
					$sqlh="select exam_name from exam where exam_id='$id'";
						$stmth=$dbh->prepare($sqlh);
						$stmth->execute();
						$rh=$stmth->fetch();
						?>
		
						<div >

							<p ><span style='position:absolute;z-index:-3;margin-left:0px;margin-top:
								34px;width:1200px;height:1580px'><img style="width:1200; height:1568"
								src="sample%20report%20-%20deepak_files/photo1.png"
								alt="Background image of a butterfly"></span>
							</p>
						</div>
						<div style='padding:500pt 36.0pt 0pt 600pt'>
							<p class="MsoTitle"><span lang=EN-US style='font-size:36.0pt'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;YOUTH MONEY
								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OLYMPIAD<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STUDENT <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REPORT
								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $rh['exam_name'];?></b></span></p>
								<p lang=EN-US style='font-size:24.0pt'><br><br><br><br><br><br><br><br><br><br><br><br><img src="sample%20report%20-%20deepak_files/photo3.png"> <br><?php echo $name;?></p>
							</div>
							
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
				
	
															<div class="div1">
							<div style='border:solid #B53D68 1.0pt;padding:4.0pt 4.0pt 4.0pt 4.0pt;
							background:#B53D68'>

							<h1><span
								lang="EN-US" style="font-size:20pt">Overview</span></h1>
							</div>
							<div id="piechart" ></div>

						
							<div style='border:solid #F1D7E0 1.0pt;padding:4.0pt 4.0pt 4.0pt 4.0pt;
							background:#F1D7E0'>

							<h2><span
								lang="EN-US" style="font-size:20pt">Overall Percentile</span></h2>
							</div>

							<span lang="EN-US" style='font-size:62.0pt;line-height:115%'><?php echo round(((($total_user-$num)/$total_user)*100),2
								);?></span>
									<div style="position:relative;left:200px;">
																<hr style="width:65%;display: block; height: 2px;
																border: 0; border-top: 2px solid #B53D68;
																"><span style="color:#BD5C7D;position:relative;left:200px;font-size:20pt"><b><i>You have scored more than <?php echo round(((($total_user-$num)/$total_user)*100),2
																);?> % of your fellow test takers</i></b></span><hr style="width:65%;display: block; height: 2px;
																border: 0; border-top: 2px solid #B53D68;
																">
															</div>
							
							<br>
							<br>
						<br>
							<br><br>
							<br><br>
							
						
							<div style='border:solid #F1D7E0 1.0pt;padding:4.0pt 4.0pt 4.0pt 4.0pt;
							background:#F1D7E0'>

							<h2><span
								lang="EN-US" style="font-size:20pt">Score Based Rank</span></h2>
							</div>
							<br>
							<br>
							<table cellspacing="40"border="0" bgcolor="#660033">
								<tr>
									<td rowspan="12">
										<p class="rotate"style="font-size:55pt; color:white"><?php if($tot
											>=95)
										$score1="A";
										else
											if($tot
												>=90)
												$score1="A-";
											else
												if($tot
													>=85)
													$score1="B+";
												else
													if($tot>=80)
														$score1="B";
													else
														if($tot>=75)
															$score1="B-";

														else
															if($tot>=70)
																$score1="C+";
															else
																if($tot>=65)
																	$score1="C";
																else
																	if($tot>=60)
																		$score1="C-";
																	else
																		if($tot>=55)
																			$score1="D+";

																		else
																			if($tot>=50)
																				$score1="D";
																			else
																				$score1="F";



																			?>
																			<?php echo $score1;?></p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">95-100</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">90-94</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">85-89</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">80-84</p>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<p style="font-size:20pt; color:white">A</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">A-</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">B+</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">B</p>
																		</td>
																	</tr>
																	<tr>


																		<td>
																			<p style="font-size:20pt; color:white">75-79</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">70-74</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">65-69</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">60-64</p>
																		</td>
																	</tr>
																	<tr>


																		<td>
																			<p style="font-size:20pt; color:white">B-</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">C+</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">C</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">C-</p>
																		</td>
																	</tr>


																	<tr>

																		<td>
																			<p style="font-size:20pt; color:white">55-59</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">50-54</p>
																		</td>
																		<td>
																			<p style="font-size:20pt; color:white">0-49</p>
																		</td>
																	</tr><tr>

																	<td>
																		<p style="font-size:20pt; color:white">D+</p>
																	</td>
																	<td>
																		<p style="font-size:20pt; color:white">D</p>
																	</td>
																	<td>
																		<p style="font-size:20pt; color:white">F</p>
																	</td>






																</tr>

															</table>
														
															</div>
															<br>
															<br>
															<br>
															<br><br>
															<br><br>
															<br><br>
															<br>
															<br>
															<br><br>
															<br><br>
															<br><br>
															<br>
															<br>
															<br>
															</div>
															<div class="div1">
															<div style='border:solid #B53D68 1.0pt;padding:4.0pt 4.0pt 4.0pt 4.0pt;
															background:#B53D68'>

															<h1><span
																lang="EN-US" style="font-size:20pt">Section Wise</span></h1>
															</div>
															<br>
															<div style='border:solid #F1D7E0 1.0pt;padding:4.0pt 4.0pt 4.0pt 4.0pt;
															background:#F1D7E0'>

															<h2><span
																lang="EN-US" style="font-size:20pt" >Scores and Averages in %</span></h2>
															</div>
															<div id="visualization" ></div>
															<br>
															<br>
															<br>
															<br><br>
															<br><br>
															<br><br>
															<br><br>
															<br><br>
															<br><br>
															<br>
															<div style='border:solid #F1D7E0 1.0pt;padding:4.0pt 4.0pt 4.0pt 4.0pt;
															background:#F1D7E0'>
															<h2><span
																lang="EN-US" style="font-size:20pt">Percentiles</span></h2>
															</div>
															<div id="chart_div" style="width: 1200px; height: 700px;"></div>
														</div>
														<script type="text/javascript">
															google.load("visualization", "1", {packages:["corechart"]});
															google.setOnLoadCallback(drawChart);
															function drawChart() {
																var data = google.visualization.arrayToDataTable([
																	['Task', 'Hours per Day'],
																	['Correct Answers',    <?php echo $tot;?>],
																	['Wrong Answers',    <?php echo 100-$tot;?>],

																	]);

																var options = {
																	title: 'Score out of 100%'
																	,colors:['#B53D68','#F1D7E0'],
																	width:1200,
																	height:700,
																	fontSize:30,
																	vAxis: {
    gridlines: {
        color: 'transparent'
    }
}
																};

																var chart = new google.visualization.PieChart(document.getElementById('piechart'));
																chart.draw(data, options);
															}
															function drawVisualization1() {
        // Create and populate the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Sections');
        data.addColumn('number', 'Percentiles');
        data.addColumn({type: 'number', role: 'annotation'});
        
        
        data.addRows(<?php echo json_encode($x1);?>);
        // Create and draw the visualization.
        var ac = new google.visualization.ComboChart(document.getElementById('chart_div'));
        ac.draw(data, {
        	title : 'Percentiles',
        	width: 1100,
        	height: 700,
        	vAxis: {title: "Percentage"},
        	hAxis: {title: "Sections"},
        	seriesType: "bars",
        	fontSize:25
        	,colors:['#B53D68'],
        	bar: {groupWidth: "75%"},
        	vAxis: {
    gridlines: {
        color: 'transparent'
    }
}
        });
    }


    google.setOnLoadCallback(drawVisualization1);
</script>
</script>


<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	
	function drawVisualization() {
        // Create and populate the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Sections');
        data.addColumn('number', 'Your Score');
        data.addColumn({type: 'number', role: 'annotation'});
        data.addColumn('number', 'College Average');
        data.addColumn({type: 'number', role: 'annotation'});
        
        data.addRows(<?php echo json_encode($x);?>);
        // Create and draw the visualization.
        var ac = new google.visualization.ComboChart(document.getElementById('visualization'));
        ac.draw(data, {
        	title : 'Score and Averages in %',
        	width: 1200,
        	height: 700,
        	vAxis: {title: "Percentage"},
        	hAxis: {title: "Sections"},
        	seriesType: "bars",
        	fontSize:30
        	,colors:['#B53D68','#F1D7E0'],
        	bar: {groupWidth: "90%"},
        	vAxis: {
    gridlines: {
        color: 'transparent'
    }
}
        });
    }


    google.setOnLoadCallback(drawVisualization);
</script>

<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

</body>
</html>
