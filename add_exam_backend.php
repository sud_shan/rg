<?php
require_once 'admin_check.php';
?>
<?php
require_once 'include/database.php';

if(isset($_POST))
{
	$exam_name=$_POST['exam_name'];
	$collegeid=$_POST['college_name'];
	
	$stmt=$dbh->prepare("insert into exam(exam_name,college_id) values(:exam_name,:college_id);");
	$stmt->bindParam(':exam_name',$exam_name);
	$stmt->bindParam(':college_id',$collegeid);
	
	if($stmt->execute())
	{

		echo "Exam Added Successfully";
	}
	else
	{

		echo "Failed";

	}
}

?>