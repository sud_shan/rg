<?php
require_once 'admin_check.php';
?>
<?php
require_once 'include/database.php';

if(isset($_POST))
{
	$exam_id=$_POST['id'];
	
	$stmt=$dbh->prepare("delete from exam where exam_id=:id");
	$stmt->bindParam(':id',$exam_id);
	
	if($stmt->execute())
	{
		$stmt=$dbh->prepare("drop table if exists performance_".$exam_id);
	$stmt->bindParam(':id',$exam_id);
	$stmt->execute();
	$stmt=$dbh->prepare("drop table if exists sections_".$exam_id);
	$stmt->bindParam(':id',$exam_id);
	$stmt->execute();
	$stmt=$dbh->prepare("drop table if exists table3_".$exam_id);
	$stmt->bindParam(':id',$exam_id);
	$stmt->execute();
	$stmt=$dbh->prepare("drop table if exists tbl_name_".$exam_id);
	$stmt->bindParam(':id',$exam_id);
	$stmt->execute();
		echo "Exam Deleted";

	}
	else
	{

		echo "Failed";

	}
}

?>