<?php
require_once 'admin_check.php';
?>
<?php
require_once 'include/database.php';
error_reporting(E_ALL);
if(isset($_POST))
{
	$college_name=$_POST['college_name'];
	$college_address=$_POST['college_address'];
	$college_city=$_POST['college_city'];
	$college_pin=$_POST['college_pin'];	
	$stmt=$dbh->prepare("insert into college(college_name,college_address,college_city,college_pin) values(:college_name,:college_address,:college_city,:college_pin);");
	$stmt->bindParam(':college_name',$college_name);
	$stmt->bindParam(':college_address',$college_address);
	$stmt->bindParam(':college_city',$college_city);
	$stmt->bindParam(':college_pin',$college_pin);
	if($stmt->execute())
	{

		echo "College Added Successfully";
	}
	else
	{

		echo "Failed";

	}
}

?>