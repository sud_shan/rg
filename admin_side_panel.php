 <aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">

            <div class="pull-left info">
                <p>Hello, Admin</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            
            <li id="college_add">
                <a href="add_college.php">
                     <span>Add College</span> 
                </a>
            </li>
            <li id="exam_add">
                <a href="add_exams.php">
                     <span>Add Exam</span> 
                </a>
            </li>
             <li id="rep_add">
                <a href="add_rep.php">
                     <span>Associate Representative of College</span> 
                </a>
            </li>
            <li id="data_upload">
                <a href="upload_data.php">
                     <span>Upload Data</span> 
                </a>
            </li>
            <li id="report_view">
                <a href="view_report.php">
                     <span>View Report</span> 
                </a>
            </li>
            <li id="link_delete">
                <a href="delete_link.php">
                     <span>Delete Student-College Link</span> 
                </a>
            </li>
            <li id="exam_delete">
                <a href="delete_exam.php">
                     <span>Delete Exams</span> 
                </a>
            </li>
             <li id="college_delete">
                <a href="delete_college.php">
                     <span>Delete College</span> 
                </a>
            </li>
          </ul>  
        </section>
        <!-- /.sidebar -->
    </aside>
