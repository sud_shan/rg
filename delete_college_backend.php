<?php
require_once 'admin_check.php';
?>
<?php
require_once 'include/database.php';

if(isset($_POST))
{
	$college_id=$_POST['id'];
	$stmt=$dbh->prepare("select * from exam where college_id=:id");
	$stmt->bindParam(':id',$college_id);
	$stmt->execute();
	while($r=$stmt->fetch())
	{
		$exam_id=$r['exam_id'];

		$stmt1=$dbh->prepare("delete from exam where exam_id=:id");
		$stmt1->bindParam(':id',$exam_id);

		$stmt1->execute();

		$stmt1=$dbh->prepare("drop table if exists performance_".$exam_id);
		$stmt1->bindParam(':id',$exam_id);
		$stmt1->execute();
		$stmt1=$dbh->prepare("drop table if exists sections_".$exam_id);
		$stmt1->bindParam(':id',$exam_id);
		$stmt1->execute();
		$stmt1=$dbh->prepare("drop table if exists table3_".$exam_id);
		$stmt1->bindParam(':id',$exam_id);
		$stmt1->execute();
		$stmt1=$dbh->prepare("drop table if exists tbl_name_".$exam_id);
		$stmt1->bindParam(':id',$exam_id);
		$stmt1->execute();




	}
	$stmt=$dbh->prepare("delete from college where college_id=:id");
	$stmt->bindParam(':id',$college_id);
	if($stmt->execute())
	{
		echo "College Deleted";
	}
	else
	{

		echo "Failed";

	}
	
}




?>